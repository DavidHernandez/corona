---
layout: post
title:  "día 9 - interior"
date:   2020-03-22
image: /corona/assets/images/interior.jpg
---
Hoy montamos un nuevo lugar de estudio y cacharros. Así que fue domingo reconstruyendo el interior. Joti dice mañana más pero ya que vamos una semana en el interior: el siguiente domingo más.

En las noticias dicen que el encierro durará hasta después de la cuarentena. En otras dicen que será el inicio de la nueva normalidad. Hoy da igual. Mirar adentro siempre es una forma de nuevos encuentros. ¿Para salir al exterior? Nuestros chats, stickers y videollamadas. En los dos espacios: el internet y mucha música.
